package com.india.jhamajham.Home;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.india.jhamajham.Main_Menu.Custom_ViewPager;
import com.india.jhamajham.Main_Menu.RelateToFragment_OnBack.OnBackPressListener;
import com.india.jhamajham.Main_Menu.RelateToFragment_OnBack.RootFragment;
import com.india.jhamajham.R;


public class MainFragmentHome extends RootFragment implements OnBackPressListener{


    View view;
    Context context;
    TextView forYouTv,followingTv;
    private Custom_ViewPager viewpager;
    public MainFragmentHome() {
        // Required empty public constructor
    }

    ViewPagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main_home, container, false);

        context=getContext();
        return init();
    }

    private View init() {

        forYouTv =view.findViewById(R.id.tv_foru);
        followingTv =view.findViewById(R.id.tv_following) ;
        viewpager = view.findViewById(R.id.viewpager);
        viewpager.setOffscreenPageLimit(2);
        viewpager.setPagingEnabled(false);

       followingTv.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Toast.makeText(context, "Keep calm. Developers working on it!\nThank you!", Toast.LENGTH_SHORT).show();
           }
       });


        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new ViewPagerAdapter(getResources(), getChildFragmentManager());
        viewpager.setAdapter(adapter);
        viewpager.setCurrentItem(0);
        followingTv.setTextColor(getResources().getColor(R.color.dimgray));
        forYouTv.setTextColor(getResources().getColor(R.color.white));

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();


        public ViewPagerAdapter(final Resources resources, FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            final Fragment result;
            switch (position) {
                case 0:
                    result = new Home_F();
                    break;
                case 1:
                result =  new Following_Video_F();
                break;
                default:
                    result = null;
                    break;            }

            return result;
        }

        @Override
        public int getCount() {
            return 2;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            registeredFragments.remove(position);

            super.destroyItem(container, position, object);

        }


        /**
         * Get the Fragment by position
         *
         * @param position tab position of the fragment
         * @return
         */
        public Fragment getRegisteredFragment(int position) {

            return registeredFragments.get(position);

        }
    }

    public boolean onBackPressed() {
        // currently visible tab Fragment
        OnBackPressListener currentFragment = (OnBackPressListener) adapter.getRegisteredFragment(viewpager.getCurrentItem());

        if (currentFragment != null) {
            // lets see if the currentFragment or any of its childFragment can handle onBackPressed
            return currentFragment.onBackPressed();
        }

        // this Fragment couldn't handle the onBackPressed call
        return false;
    }


}