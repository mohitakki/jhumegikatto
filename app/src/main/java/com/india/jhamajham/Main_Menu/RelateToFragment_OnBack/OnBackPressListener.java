package com.india.jhamajham.Main_Menu.RelateToFragment_OnBack;

/**
 * Created by Dapper on 08/07/2020
 */

public interface OnBackPressListener {
    public boolean onBackPressed();
}
