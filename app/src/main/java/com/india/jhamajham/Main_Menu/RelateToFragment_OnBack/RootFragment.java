package com.india.jhamajham.Main_Menu.RelateToFragment_OnBack;

import androidx.fragment.app.Fragment;

/**
 * Created by Dapper on 08/07/2020
 */

public class RootFragment extends Fragment implements OnBackPressListener {

    @Override
    public boolean onBackPressed() {
        return new BackPressImplimentation(this).onBackPressed();
    }
}